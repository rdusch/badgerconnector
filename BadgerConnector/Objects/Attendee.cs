﻿namespace com.okuma.Badger.Objects
{
    public class Attendee
    {
        public int Id { get; set; }
        public string BadgeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string Division { get; set; }
        public int Distributor_Id { get; set; }
        public Distributor Distributor { get; set; }
        public int Address_Id { get; set; }
        public Address Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string RegistrationType { get; set; }
        public string Notes { get; set; }
        public int Event_Id { get; set; }
        public Event Event { get; set; }
    }
}
