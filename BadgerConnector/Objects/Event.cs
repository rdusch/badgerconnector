﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.okuma.Badger.Objects
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Address_Id { get; set; }
        public Address Address { get; set; }
        public string AuthKey { get; set; }
        public string ExhibitId { get; set; }
        public string EventCode { get; set; }
        public string URl { get; set; }
    }
}
