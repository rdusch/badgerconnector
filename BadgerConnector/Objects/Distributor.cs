﻿namespace com.okuma.Badger.Objects
{
    public class Distributor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Division { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string PostalCode { get; set; }
    }
}
