﻿using com.okuma.Badger.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace com.okuma
{
    public class MissingScanFieldException : Exception
    {
        public MissingScanFieldException(string FieldName) : base("The field `" + FieldName + "` (not found) must be included in a scan string dictionary.") { }
    }

    public static class BadgerConnector
    {
        private static string _BadgerCloudUrl;
        internal static string BadgerPortalAddress_SubmitScan;
        internal static string BadgerPortalAddress_GetShowList;
        internal static string BadgerPortalAddress_AddAttendeeComment;
        public static Attendee Attendee { get; private set; }
        public static readonly string SCAN_DATA_STRING = "SCAN_DATA_STRING";
        public static readonly string LOCATION_ALIAS = "LOCATION_ALIAS";
        public static readonly string DEMO_NAME = "DEMO_NAME";
        public static readonly string LOCAL_SCAN_TIME = "LOCAL_SCAN_TIME";
        public static readonly string APP_VERSION = "APP_VERSION";
        public static readonly string COMMENTS = "COMMENTS";
        public static readonly string QUESTION_LIST = "QUESTION_LIST";
        private static string[] MandatoryFieldList;
        private const string ALL_IS_WELL = "OK#";

        private static readonly string TOKEN = Application.Default.BadgerToken;

        public static string BadgerCloudUrl
        {
            get
            {
                return _BadgerCloudUrl;
            }

            set
            {
                _BadgerCloudUrl = value;
                BadgerPortalAddress_SubmitScan = value + "/scannerconnect/leadlookup";
                BadgerPortalAddress_GetShowList = value + "/scannerconnect/getshowlist";
                BadgerPortalAddress_AddAttendeeComment = value + "/scannerconnect/UpdateAttendee";
            }
        }

        static BadgerConnector()
        {
            // ACCEPT ANY AND ALL SSL CERTIFICATES INCLUDING FAKE ONES!!!!!  BAD BAD BAD BAD BAD BAD BAD but whatever !!!!!!!
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        }

        public static List<string> GetShowList()
        {
            try
            {
                var response = MakeRequest(new { Token = TOKEN }, BadgerPortalAddress_GetShowList);
                var eventList = ((Dictionary<string, List<Event>>)(new JavaScriptSerializer()).Deserialize(response, typeof(Dictionary<string, List<Event>>)))["showList"];

                var list = new List<string>();
                foreach (Event e in eventList)
                {
                    list.Add(e.Name);
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SubmitScan(Dictionary<string, string> ScanDictionary)
        {
            Attendee = null;
            try
            {
                MandatoryFieldList = new string[] { SCAN_DATA_STRING, LOCATION_ALIAS, DEMO_NAME, LOCAL_SCAN_TIME, APP_VERSION, COMMENTS };
                CheckForMissingFields(ScanDictionary);
                var response = MakeRequest(new
                {
                    Token = TOKEN,
                    ScanData = ScanDictionary[SCAN_DATA_STRING],
                    LocationAlias = ScanDictionary[LOCATION_ALIAS],
                    DemoName = ScanDictionary[DEMO_NAME],
                    LocalScanTime = ScanDictionary[LOCAL_SCAN_TIME],
                    Comments = ScanDictionary[COMMENTS]
                }, BadgerPortalAddress_SubmitScan);

                Attendee = ParseResponse(response);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AddAttendeeComment(Dictionary<string, string> ScanDictionary)
        {
            try
            {
                MandatoryFieldList = new string[] { SCAN_DATA_STRING, COMMENTS };
                CheckForMissingFields(ScanDictionary);
                var questionList = string.Empty;
                if (ScanDictionary.ContainsKey(QUESTION_LIST))
                {
                    questionList = ScanDictionary[QUESTION_LIST];
                }

                object param = new
                {
                    Token = TOKEN,
                    ScanData = ScanDictionary[SCAN_DATA_STRING],
                    Comments = ScanDictionary[COMMENTS],
                    QuestionList = questionList
                };

                var response = MakeRequest(param, BadgerPortalAddress_AddAttendeeComment);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static Attendee ParseResponse(string response)
        {
            try
            {
                return (Attendee)(new JavaScriptSerializer().Deserialize(response, typeof(Attendee)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void CheckForMissingFields(Dictionary<string, string> scanDictionary)
        {
            foreach (string field in MandatoryFieldList)
            {
                if (!scanDictionary.ContainsKey(field))
                {
                    throw new MissingScanFieldException(field);
                }
            }
        }

        private static string MakeRequest(object ScanDictionary, string uri)
        {
            string response = null;
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "POST";
            request.ContentType = "application/json";
            var streamWriter = request.GetRequestStream();
            var postData = new JavaScriptSerializer().Serialize(ScanDictionary);
            var data = Encoding.ASCII.GetBytes(postData);
            streamWriter.Write(data, 0, data.Length);

            var httpResponse = (HttpWebResponse)request.GetResponse();

            var streamReader = new StreamReader(httpResponse.GetResponseStream());
            response = streamReader.ReadToEnd().ToString();
            streamReader.Close();
            streamWriter.Close();
                        
            return response;
        }
    }
}