﻿using com.okuma;
using System;
using System.Collections.Generic;
using System.Windows;

namespace BadgerConnectorTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent(); 
            BadgerConnector.BadgerCloudUrl = "http://localhost:5000";
            //BadgerConnector.BadgerCloudUrl = "http://badgerdevtest.azurewebsites.net";
            //BadgerConnector.BadgerCloudUrl = "http://okumabadger.azurewebsites.net";
        }

        private void ShowList_Click(object sender, RoutedEventArgs e)
        {
            lbResponseDeserialized.ItemsSource = null;
            tbLastSerialized.Content = "Data sent: NULL";

            try
            {

                var response = BadgerConnector.GetShowList();
                tbLastReceived.Content = "Server response: " + response.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void SendScan_Click(object sender, RoutedEventArgs e)
        {
            lbResponseDeserialized.Visibility = System.Windows.Visibility.Hidden;
            lbResponseDeserialized.ItemsSource = null;
            tbLastReceived.Content = null;

            var testData = new Dictionary<string, string> {
                    {BadgerConnector.SCAN_DATA_STRING, "220697$IMTS0916$ANDY$MURTY$HARDINGE INC.$"},
                    {BadgerConnector.LOCATION_ALIAS, "SERVICE_2"},
                    {BadgerConnector.DEMO_NAME, "DEMO_NAME"},
                    {BadgerConnector.LOCAL_SCAN_TIME, DateTime.Now.ToString() },
                    {BadgerConnector.APP_VERSION, "APP_VERSION"},
                    {BadgerConnector.COMMENTS, "COMMENTS"}
            };

            tbLastSerialized.Content = "Data sent: " + testData.ToString();
            try
            {
                
                var response = BadgerConnector.SubmitScan(testData);

                lbSendScanRetval.Content = response.ToString();
                tbLastReceived.Content = BadgerConnector.Attendee.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddAttendeeComment_Click(object sender, RoutedEventArgs e)
        {
            var testData = new Dictionary<string, string> {
                    {BadgerConnector.SCAN_DATA_STRING, "$IMTS0916$FirstName$Last$Company$"},
                    {BadgerConnector.COMMENTS, this.txtComment.Text}
            };

            try
            {
                var response = BadgerConnector.AddAttendeeComment(testData);

                lbSendScanRetval.Content = response.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SendQuestionList_Click(object sender, RoutedEventArgs e)
        {
            var testData = new Dictionary<string, string> {
                {BadgerConnector.SCAN_DATA_STRING, "200000$IMTS0916$FirstName$Last$Company$"},
                {BadgerConnector.COMMENTS, "Comment"},
               // {BadgerConnector.QUESTION_LIST, "<?xml version=\"1.0\" encoding=\"utf-16\"?> <ArrayOfStoredQuestion xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"> <StoredQuestion> <Index>1</Index> <Question>How many CNC machines does your company currently own? </Question> <Answer>0-5;</Answer> </StoredQuestion> <StoredQuestion> <Index>5</Index> <Question>What sector is your company classified?</Question> <Answer>Job Shop;</Answer> </StoredQuestion> <StoredQuestion> <Index>10</Index> <Question>Are you presently an Okuma owner?</Question> <Answer>False</Answer> </StoredQuestion> <StoredQuestion> <Index>15</Index> <Question>How do you perform service on your Okuma product</Question> <Answer>Distributor;</Answer> </StoredQuestion> </ArrayOfStoredQuestion>"}
               {BadgerConnector.QUESTION_LIST, "<?xml version=\"1.0\" encoding=\"utf-16\" ?><ArrayOfStoredQuestion xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><StoredQuestion><Index>1</Index><Question>How many CNC machines does your company currently own?</Question> <Answer>0-5;</Answer>  </StoredQuestion> <StoredQuestion> <Index>5</Index> <Question>What sector is your company classified?</Question> <Answer>Medical;</Answer> </StoredQuestion><StoredQuestion><Index>10</Index> <Question>Are you presently an Okuma owner?</Question> <Answer>False</Answer> </StoredQuestion><StoredQuestion><Index>15</Index> <Question>How do you perform service on your Okuma product</Question> <Answer>Internal;</Answer> </StoredQuestion><StoredQuestion><Index>20</Index> <Question>Where do you procure your after-market service parts?</Question> <Answer>Third Party;</Answer> </StoredQuestion><StoredQuestion><Index>25</Index> <Question>Who performs Preventive Maintenance on your Okuma Product?</Question> <Answer>Internal;</Answer> </StoredQuestion><StoredQuestion><Index>30</Index> <Question>Would you like our service group to contact you? If so, please select all that apply.</Question> <Answer>General;</Answer> </StoredQuestion><StoredQuestion><Index>40</Index> <Question>Do you have any suggestions to improve our after-market support?</Question> <Answer /> </StoredQuestion> </ArrayOfStoredQuestion>"}
            };

            try
            {
                var response = BadgerConnector.AddAttendeeComment(testData);

                lbSendScanRetval.Content = response.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
